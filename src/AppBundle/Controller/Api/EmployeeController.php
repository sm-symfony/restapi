<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeController extends Controller
{
    /**
     * @Route("/api/employees", methods={"GET"}, name="api_employee_list")
     */
    public function listAction()
    {
        $employees = $this->getDoctrine()
            ->getRepository('AppBundle:Employee')
            ->findAll();

        $data = ['employees' => []];

        foreach ($employees as $employee) {
            $data['employees'][] = $this->serializeEmployee($employee);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/employees", methods={"POST"}, name="api_employee_new")
     */
    public function newAction(Request $request)
    {
        // {"firstName":"Sushil","lastName":"Meher","city":1}

        $body = $request->getContent();
        $data = json_decode($body, true);

        $employee = new Employee();
        $form = $this->createForm('AppBundle\Form\EmployeeFormType', $employee);
        $form->submit($data);

        if (!$form->isValid()) {
            $errors = $this->getErrorsFromForm($form);
            $data = [
                'type' => 'validation_error',
                'title' => 'There was a validation error',
                'errors' => $errors
            ];
            return new JsonResponse($data, 400);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($employee);
        $em->flush();

        $location = $this->generateUrl('api_employee_show', [
            'firstName' => $employee->getFirstName()
        ]);
        $data = $this->serializeEmployee($employee);
        $response = new JsonResponse($data, 201);
        $response->headers->set('Location', $location);
        return $response;
    }

    /**
     * @Route("/api/employees/{firstName}", methods={"GET"}, name="api_employee_show")
     */
    public function showAction($firstName)
    {
        $employee = $this->getDoctrine()
            ->getRepository('AppBundle:Employee')
            ->findOneBy(['firstName' => $firstName]);

        if (!$employee) {
            throw $this->createNotFoundException('No employee found with first name ' . $firstName);
        }

        $data = $this->serializeEmployee($employee);

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/employees/{id}", methods={"PUT"}, name="api_employee_update")
     */
    public function updateAction($id)
    {
        return new Response('Employee Update EndPoint');
    }

    /**
     * @Route("/api/employees/{id}", methods={"DELETE"}, name="api_employee_delete")
     */
    public function deleteAction($id)
    {
        return new Response('Employee Delete EndPoint');
    }

    public function serializeEmployee(Employee $employee)
    {
        return [
            'firstName' => $employee->getFirstName(),
            'lastName' => $employee->getLastName(),
            'city' => $employee->getCity()->getName()
        ];
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}