<?php


namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\City;
use AppBundle\Entity\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        // Create 10 cities
        for ($i = 0; $i < 10; $i++) {
            $city = new City();
            $city->setName($faker->unique()->city);
            $manager->persist($city);
        }

        $manager->flush();

        // Get all cities
        $cities = $manager->getRepository('AppBundle:City')
            ->findAll();

        // Create 50 employees
        for ($i = 0; $i < 50; $i++) {
            $employee = new Employee();
            $employee->setFirstName($faker->firstName);
            $employee->setLastName($faker->lastName);
            $employee->setCity($faker->randomElement($cities));
            $manager->persist($employee);
        }

        $manager->flush();
    }
}