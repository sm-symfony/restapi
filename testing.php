<?php

require __DIR__ . '/vendor/autoload.php';

$client = new GuzzleHttp\Client([
    'base_uri' => 'http://localhost:8000',
    'defaults' => [
        'exceptions' => false
    ]
]);

$faker = \Faker\Factory::create();
$data = [
    'firstName' => $faker->firstName,
    'lastName' => $faker->lastName,
    'city' => 1
];

// POST to create employee
$response = $client->post('/api/employees', [
    'body' => json_encode($data)
]);

$employeeUrl = $response->getHeader('Location')[0];

// GET fetch that employee
$response = $client->get($employeeUrl);

// GET fetch collection
$response = $client->get('/api/employees');

echo $response->getBody()->getContents();